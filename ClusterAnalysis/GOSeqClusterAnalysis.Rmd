Gene Ontology Enrichment Analysis of STEM-identified clusters
========================================================

Introduction
------------

STEM analysis of DE genes identified four significiant gene expression profiles, including:

* earlyhigh - cluster 1
* latehigh - cluster 6
* greenlow - cluster 2
* greenhigh - cluster 0

Question:

* Which categories are over-represented among clusters 0, 1, 2, and 6?

Analysis
--------

### GO terms

Read GO annotation file for blueberry:

```{r}
gene2go='../ManuscriptsSupplementalDataFiles/SupplementalDataFiles/S3-GO/S3-GO.tsv'
gene2go=read.delim(gene2go,sep='\t',header=T)[,c('gene','id')]
names(gene2go)=c('gene','category')
```

### Transcript sizes

Read transcript sizes for blueberry genes:

```{r}
fname='../GeneModelAnalysis/results/tx_size.txt.gz'
sizes=read.delim(fname,header=T,sep='\t')
sizes$kb=sizes$bp/1000
locus_names=sizes$locus
sizes=sizes$kb
names(sizes)=locus_names
rm(locus_names)
```

### Cluster results

Read results from clusters file:

```{r}
fname='results/StemResults10Clusters.tsv.gz'
results=read.delim(fname,skip=1,sep='\t',header=T,as.is=T)
profile=results[,'Profile']
names(profile)=results$gene
```

How many genes were in each cluster?

```{r}
table(profile)
```

### Genes used for clustering

```{r}
fname='../DiffExpAnalysis/results/scaledAveRPKM-DE.tsv'
gene.vector=read.delim(fname,quote='',as.is=T,header=T)[,1]
names(gene.vector)=gene.vector
```

### All the genes

```{r}
fname='../PlantCyc/results/V_corymbosum_scaffold_May_2013_wDescrPwy.bed'
genes=read.delim(fname,as.is=T,header=F,quote='')[,13:1]
names(genes)=c('gene','descr')
genes=genes[!duplicated(genes$gene),]
gene.vector=genes[,'gene']
names(gene.vector)=genes$gene
```

FDR cutoff for GO term significance:

```{r}
go_FDR=0.001 
```

Load GOSeq library:

```{r}
suppressPackageStartupMessages(library(goseq))
```

Define function for GO analysis:

```{r}
do.go=function(genes=NULL,profile=NULL,
               length.vector=NULL,clusters=NULL,
               index=NULL,gene2go=NULL,
               go_defs=NULL,fname=NULL,
               go_FDR=NULL){
  cluster_num=clusters[index]
  cluster_name=names(clusters)[index]
  indexes=which(profile==cluster_num)
  select=names(profile)[indexes]
  gene.vector=rep(0,length(genes))
  names(gene.vector)=names(genes)
  gene.vector[select]=1
  pwf<-nullp(gene.vector,
             bias.data=length.vector[names(gene.vector)],
             plot.fit=FALSE)
  GO=goseq(pwf,gene2cat=gene2go,method="Wallenius",
           use_genes_without_cat=TRUE)
  names(GO)[2]='FDR.over_represented'
  names(GO)[3]='FDR.under_represented'
  GO$FDR.over_represented=p.adjust(GO$FDR.over_represented,method='BH')
  GO$FDR.under_represented=p.adjust(GO$FDR.under_represented,method='BH')

  GO$cluster_num=rep(cluster_num,nrow(GO))
  GO$cluster_name=rep(cluster_name,nrow(GO))
  v=union(which(GO$FDR.over_represented<=go_FDR),which(GO$FDR.under_represented<=go_FDR))
  GO=GO[v,c('cluster_num','cluster_name','category','ontology','term','FDR.over_represented','FDR.under_represented','numDEInCat','numInCat')]
  o=order(GO$FDR.over_represented,decreasing=F)
  GO[o,]
}
clusters=c(1,6,2,0)
names(clusters)=c('earlyhigh','latehigh','greenlow','greenhigh')
index=1
```


### Cluster `r names(clusters)[index]`

               
```{r}
GO=do.go(genes=gene.vector,profile=profile,
         length.vector=sizes,
         index=index,clusters=clusters,
         gene2go=gene2go,go_FDR=go_FDR)
all=GO
GO[,c('term','FDR.over_represented','FDR.under_represented','numDEInCat','numInCat')]
```

Genes involved in photosynthesis, cell cycle, cell growth, and metabolism were unusually abundant in the early `r names(clusters)[index]` cluster. This reflects active cell division and cell growth processes underway in this sample type.

### Cluster `r names(clusters)[index+1]`

```{r}
index=2
GO=do.go(genes=gene.vector,profile=profile,
         length.vector=sizes,
         index=index,clusters=clusters,
         gene2go=gene2go,go_defs=go_defs,
         fname=NULL,go_FDR=go_FDR)
all=rbind(all,GO)
GO[,c('term','FDR.over_represented','FDR.under_represented','numDEInCat','numInCat')]
```

The `r names(clusters)[index]` cluster is enriched with genes involved in transport, carbohydrate metabolism, and enzymatic activity but has unusually few DE genes involved in DNA/RNA/protein binding and DNA metabolism. This highlights how fruit cells are metabolically active and are importing sugars but are not dividing.

### Cluster `r names(clusters)[index+1]`

```{r}
index=3
GO=do.go(genes=gene.vector,profile=profile,
         length.vector=sizes,
         index=index,clusters=clusters,
         gene2go=gene2go,go_defs=go_defs,
         fname=NULL,go_FDR=go_FDR)
all=rbind(all,GO)
GO[,c('term','FDR.over_represented','FDR.under_represented','numDEInCat','numInCat')]
```

The `r names(clusters)[index]` has unusually few DE genes with the function protein binding and no enriched genes.

### Cluster `r names(clusters)[index+1]`

```{r}
index=4
GO=do.go(genes=gene.vector,profile=profile,
         length.vector=sizes,
         index=index,clusters=clusters,
         gene2go=gene2go,go_defs=go_defs,
         fname=NULL,go_FDR)
all=rbind(all,GO)
GO
```

The `r names(clusters)[index]` cluster is enriched with genes involved in metabolism, catabolism, and lipid binding and with genes localized to the membrane, cell wall, and extracellular region. There are unusually few genes involved in RNA/DNA binding. This suggests that in green fruit, proteins involved in cell-wall related processes may be up-regulated, while processes related to replication and RNA processing are suppressed.

### Write results

```{r}
fname='results/clusterGO.tsv'
write.table(all,file=fname,sep='\t',row.names=F,
            quote=F)
fname='../ManuscriptsSupplementalDataFiles/SupplementalDataFiles/S5-GO-Cluster/S5-GO-Cluster.tsv'
write.table(all,file=fname,sep='\t',row.names=F)
system(paste('gzip -f',fname))
```

Conclusions
-----------

Many of the significant terms represent very broad categories. The mix of categories in each cluster (compared to all the other genes) suggests that the each cluster represents a diversity of functions and processes. 

There was a clear difference in the functions and processes in the early versus late clusters.

Session info
------------

```{r}
sessionInfo()
```