#!/usr/bin/env python

ex=\
"""
To assess whether a hit to a database entry is real, we need a summarized
value for the overall percent identity of the entire hit. However, percent
identity is only reported for individual HSPs.

This script calculates a weighted average percent identity for the entire
hit, using the length of the region covered in the subject (the matched
protein sequence) to weight percent identity.

One limitation: sometimes HSPs overlap and some amino acids will contribute
more than once to the total average.
"""

# fields
# 0 database 
# 1 q.id
# 2 q.length
# 3 q.frame
# 4 q.start
# 5 q.end
# 6 s.length
# 7 s.start
# 8 s.end
# 9 identity
# 10 evalue
# 11 bit.score
# 12 s.acc.ver
# 13 s.title
qidF=1
sidF=12
iF=9
sstartF=7
sendF=8
titleF=13
slenF=6

# to test
# cut -f 2,13,10,15
import sys,argparse,fileinput

def main(files=None):
    d = {}
    sep='\t'
    onfirst=True
    for line in fileinput.input(files):
        if onfirst:
            sys.stdout.write(line.rstrip()+'\tave.identity\n')
            onfirst=False
            continue
        toks=line.rstrip().split('\t')
        qid=toks[qidF]
        sid=toks[sidF]
        title=toks[titleF]
        if title=='NA':
            toks.append('NA')
            newline=sep.join(toks)+'\n'
            sys.stdout.write(line)
            continue
        else: # store the hit details
            if not d.has_key(qid):
                d[qid]={}
                d[qid][sid]=[toks]
            elif not d[qid].has_key(sid):
                d[qid][sid]=[toks]
            else:
                d[qid][sid].append(toks)
    for qid in d.keys():
        for sid in d[qid].keys():
            hsps=d[qid][sid]
            title=hsps[0][titleF]
            totalIdentity=0
            totalLength=0
            for hsp in hsps:
                start=int(hsp[sstartF])
                end=int(hsp[sendF])
                length=end-start+1
                identity=float(hsp[iF])
                totalLength=totalLength+length
                totalIdentity=totalIdentity+(identity*length)
            totalIdentity=totalIdentity/totalLength
            for hsp in hsps:
                line='\t'.join(hsp)+'\t'+str(totalIdentity)+'\n'
                sys.stdout.write(line)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=ex)
    parser.add_argument('files', metavar='FILE', nargs='*',
                        help='BLASTX output file, tab-delimited')
    args = parser.parse_args()
    main(files=args.files)


