#!/usr/bin/env/python
"""
An attempt (not finished) to select the most descriptive blast hit
subject description to annotate blueberry transcripts.
"""

less_info=['Uncharacterized protein', # T cacao
           'PREDICTED: uncharacterized protein', # F vesca, S lycopersicum, V vinifera
           'predicted protein [Populus trichocarpa]',
           'unnamed protein product', # O sativa japonica, V vinifera
           'hypothetical protein', # P persica,  M truncatula
           'conserved hypothetical protein', # R communis
           'Putative polyprotein, related']  # Asparagus officinalis

import sys,argparse

def main():
    pass


if __name__ == '__main__':
    main()
           
