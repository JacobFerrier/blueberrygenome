Hierarchical clustering and PCA analysis of splicing trends in blueberry
========================================================

Introduction
------------

Splicing differences between different fruit development and ripening stages were identified in the markdown document titled "BlueberryAltsplice.Rmd." Inspection of the lists of differentially spliced genes suggested that for some genes, there were ripening related trends, but it was hard to visualize these using numbers only. In this markdown, we'll explore these trends further and also determine higher-level similarities in splicng between sample types.

Questions we aim to answer:

* Which samples are most or least similar to each other with respect to splicing patterns? For this, we'll use %S, the percentage of read support for the shorter variant produced by an alterantive splicing event.
* Are there trends with respect to splicing, such as genes where %S always goes up or down in a particular stage? 

Read the data:

```{r}
fname='results/S.tsv.gz'
d=read.delim(fname,as.is=T)
row.names(d)=d$as.id
# eliminates rows with NA
v=apply(d,1,function(x)sum(any(is.na(x)))== 0)
d2=d[v,]
```

Cluster samples by %S using hierarchical clustering:

```{r fig.width=7,fig.height=7}
i=grep('P',names(d2))
dataFrame=t(d2[,i])
distAS=dist(dataFrame)
hClustering=hclust(distAS)
par(las=1)
plot(hClustering,main='',xlab='')
# save to Figure folder as tiff and PNG
pic.fname=file.path('..','ManuscriptsSupplementalDataFiles',
                    'Figures',
                    'Figure 11 Alternative Splicing ArabiTag',
                    'Rplots','hclust.tiff')
quartz(file=pic.fname,width=5,height=5,dpi=600,type="tiff")
plot(hClustering)
plot(aves,main='',xlab='')
dev.off()
```


Get the genes that had differential splicing between at least one pair of stages:

```{r}
fname='results/bberry_altsplice.15.10.5.tsv.gz'
alts=read.delim(fname,as.is=T)
alts=alts[,c('as.id','gene','p')]
o=order(alts$p)
alts=alts[o,]
alts=alts[!duplicated(alts$as.id),]
```

Select rows where at least one pair of conditions had differnetially splicing:

```{r}
d2=d[alts$as.id,]
```

Do hierarchical clustering using data from differential splicing:


```{r fig.width=7,fig.height=7}
dataFrame=t(d2[,i])
distAs=dist(dataFrame)
hClustering=hclust(distAs)
plot(hClustering)

```

