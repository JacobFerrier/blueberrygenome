Contributors:

Ann Elizabeth Loraine (most of the code, RNA-Seq data analysis)
Vikas Gupta (GO annotations, blast against nr, data analysis)
April Dawn Roberts Estrada (sample collection, libraries, data analysis & interpretation)
Ivory Clabaugh Blakley (bixin pathway analysis, code review)
Ketan Patel (sample collection, libraries)

Ann has curated the code and data files so that's why you only
see her commits to this repo. 
