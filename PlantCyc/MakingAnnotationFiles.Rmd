PlantCyc Functional Annotation
=============================

Background
----------

We performed RNA-Seq on five stages of blueberry development and ripening, aligned the reads onto the May 2013 draft blueberry genome assembly, and then used Cufflinks and ab initio gene finders to make blueberry gene models. 

One goal was to identify pathways and biological processes that are important
in berry fruit development and ripening. We were particularly interested in identifying pathways of secondary metabolism that produce beneficial compounds, such as reveratrol or others flavonoids that seem to have a positive effect on human or animal health.

However, to achieve this goal, we needed to determine which of the newly discovered blueberry genes encode enzymes of secondary metabolism.

To associate blueberry genes with pathway enzymes, we obtained a curated database of plant enzymes called PlantCyc. We then used blastx to compare the blueberry genes to sequences in the database.

This markdown document describes exploratory data analysis of the results and how we used these data to improve blueberry gene functional annotations.

*Note:* This code assumes the code in the BlastxAnalysis directory has been run; it depends on the annotation bed file created there.

Goal of this analysis
---------------------

Associate blueberry genes with pathways and then use these associations to:

* Make it easier for people to search for genes of interest by keyword in IGB
* Identify up or down-regulated pathways in the blueberry RNA-Seq data set

In this markdown file, we'll perform annotation of blueberry genes and gene models using results from the blastx-based comparison to blueberry gene
models to plantcyc enzymes.

Data Processing
------------

First, we'll load a tab-delimited file containing results from the blastx search. Next, we'll add more columns to the data set, using some python scripts. For this, we'll use the pipe command from the utils library to run a UNIX command that will uncompress and stream the data to scripts that calculate and add subject percent coverage, percent identity, subject identifiers, and species of origin. The scripts resides in a subdirectory named "src."

**Percentage of subject covered** is the percentage of amino acid residues in the matched PlantCyc enzyme that was "covered" by the alignment with the blueberry query sequence either by a amino acid or a gap character. The reason we care about the amount of the subject that was matched is that if a blueberry gene aligns to the full (or close to the full) length of a PlantCyc enzyme, then we are more confident that the enzyme is homologous to the blueberry gene and that the blueberry gene does encodes a form of that enzyme. However, if only part of the enzyme is covered, then the hit may be due to a shared domain could be present in many different proteins with diverse functions.

**Summarized percent identity** is a weighted average of percent identities for HSPs in a hit. Percent identities are weighted by the length of the subject covered in the HSP. This is because there can be many HSPs (aligned regions) between a query blueberry sequence and its corresponding "best hit" from PlantCyc. A longer HSP with a higher percent identity should count more than a shorter HSP HSP with a worse percentage identity. 

The scripts that computes percentage of subject coverage and summarized percent identity reads the alignments data for each "hit", which may include multiple HSPs. The data file lists one HSP per row. Since we need to consider all the HSPs together as one hit, we use them all the calculate the %subject coverage and %identities and then adds these values as a new column to the data file. Thus, every HSP belonging to the same "hit" will get the same value in the %subject coverage and %average percent identity column. 

**Warning** A lot of the fasta headers in the PlantCyc BLAST databases had quotes in them, which breaks the R parser unless we turn off quotes when reading the data.

Also, note that the blastx was run using parameters that ensured only one subject sequence (hit) would be reported per query. This means that for each query there is one or no hit. We'll assume that is the best match for that query.

So before we get started, let's do some sanity checking on the data files containing blastx results.

Sanity checking
---------------

Get a list of all gene model ids from the the blueberry transcripts bed file, which is output from NrBlastxAnalysis.Rmd in BlastxAnalysis:

```{r}
bedfile='../BlastxAnalysis/results/V_corymbosum_scaffold_May_2013_withDescr.bed.gz'
models=read.delim(bedfile,header=F,sep='\t',as.is=T)
col.names = c("seq", "start", "end", "name", "score", "strand", "tr.start", "tr.end", "rgb", "exons", "exon.lens", "exon.starts", "gene", "producer")
names(models) = col.names
```

There were `r dim(models)[1]` models and `r length(unique(models$gene))` genes in annotation file `r bedfile`.

Read the blastx annotations:

```{r}
fname='data/big_blastx_plantcyc_results.tsv.gz'
d=read.delim(fname,header=T,sep='\t',quote='')
```

Note that EVERY sequence that was searched should be represented in the file. Sequences that had no "hit" to a protein in the database will have NA values in fields corresponding to matches.

There were `r length(unique(d$q.id))` unique queries in the blastx results file, which had `r nrow(d)` rows of data. 

Looks like the blastx results file and the annotations file are consistent in that both have the same number of query ids/gene models ids.

Data Loading
------------

Now we need to read the blastx results and feed the data through the data munging scripts 

* addSubjectCoverage.py
* addSummarizedIdentity.py
* addSubjectFields.py:

```{r}
library(utils)
cmd='gunzip -c data/big_blastx_plantcyc_results.tsv.gz | ./src/addSubjectCoverage.py | ./src/addSummarizedIdentity.py | ./src/addSubjectFields.py'
d=read.delim(pipe(cmd),header=T,sep='\t',
             na.strings='NA',quote='',as.is=T)
d$ave.identity=round(d$ave.identity,digits=1)
d$s.cov=round(d$s.cov,digits=1)
```

Sanity checking
---------------

After adding the subject coverage and subject id fields, the blueberry plantcyc blastx output file contains rows for `r length(unique(d$q.id))` queries.

Filtering hits by subject coverage
----------------------------------

We got the same number of query ids after parsing. Let's now proceed to filtering based on subject coverage and overall percent identity.

Set some constants:

```{r}
s=60
p=45
```

Choosing parameters is a black art. I'm not sure what to choose, but I've run this code many times, and what I've learned is that when I get down to the gene and single pathway level, I can tolerate false positives so long as I can see the percent identity and subject coverage in the annotations of individual genes. 

Filter based on %subject coverage `r s` and percent identity `r p`.

```{r}
hits=d[!is.na(d$s.title) & d$s.cov>=s & d$ave.identity>=p,]
```

Following the filtering step, we have hits for `r length(unique(hits$q.id))` berry sequences with %s `r s` and average percent identity `r p`. 

Let's remove duplicates so that there is only one row per query. At this point, there may be multiple lines for each hit where rows correspond to HSPs. We don't need all these extra rows, so get rid of them:

```{r}
isdup=duplicated(hits$q.id)
hits=hits[!isdup,]
```

Data Analysis
-------------

Look at the number of top hits by species. This is mostly a sanity checking exercise; we know from other work that most of the best hits should come from grape.


```{r}
tab=table(hits$species)
o=order(tab,decreasing=T)
tab[o]
```

As we've seen before, grape has the largest number of best matches, followed by poplar, cassava (Manihot esculenta), papaya, and so on. 

Combining hits with PlantCyc pathway annotations
------------------------------------------------

We have a file from PlantCyc that lists enzymes, their protein ids, and the pathways those enzymes are associated with. For the next step, link enzymes with their pathways:

```{r}
pwyfname='data/cyc_pathways.tsv.gz'
pwys=read.delim(pwyfname,header=T,sep='\t',quote='',as.is=T,na.strings=c('-'))
hits=merge(hits,pwys,by.x='s.id',
                        by.y='Protein.id',
                        all.x=T) 
```

After merging the hits and pathways data frame, we have `r nrow(hits)`.

This is more than what we started out with. This is because some enzymes are associated with multiple pathways. 

Merge with blueberry gene ids:

```{r}
tx2gene=models[,c('name','gene')]
hits=merge(hits,tx2gene,by.x="q.id",by.y="name")
tokeep = c("s.id", "ph.id", "q.id", "q.length", 
           "s.length", "ave.identity", "s.cov", 
           "protein", "species")
tokeep=append(tokeep,'gene',after=3)
cols=c(tokeep,'Pathway.id','Pathway.name',
              'Reaction.id',
              'EC','cyc')
hits=hits[,cols]
```

Write out a file:

```{r}
pwyfile=paste('results/filtered_plantcyc_results',
              s,p,sep='_')
pwyfile=paste0(pwyfile,'.tsv')
write.table(hits,
            pwyfile,sep='\t',row.names=F,quote=F)
system(paste('gzip -f',pwyfile))
```

Now that we've combined the data frames with gene information, count the number of genes that had a hit.

Following filtering, there were

* `r length(unique(hits$gene))` blueberry genes with a hit with %scov >= `r s`

How many enzymes, pathways, and reactions did we identify?

```{r}
reactions=unique(hits$Reaction.id)
pathways=unique(hits$Pathway.id)
pathway.names=unique(hits$Pathway.name)
ecs=unique(hits$EC)
```

The blastx analysis matched blueberry transcripts with `r length(reactions)` reactions, `r length(pathways)` pathways, and `r length(ecs)` EC numbers.

Preparing file for enrichment (GOSeq) analysis
----------------------------------------------

For GOSeq analysis, we'll need a mapping between blueberry genes ids and pathway identifiers.

For this, we'll use the gene models data frame we made previously. We'll use fields 4 and 13 of the models data frame, which contain the gene model/transcript id and locus id respectively for each gene model. In our blastx data, the q.id field corresponding to the gene model/transcript id. 

```{r}
cols=c('gene','protein','Pathway.id','Pathway.name')
locus2pwy=hits[,cols]
# some proteins in PlantCyc don't have a corresponding
# pathway id. let's remove those:
locus2pwy=locus2pwy[!is.na(locus2pwy$Pathway.id),]
# lets also order the data frame by locus and Pathway.id
# so that if a locus has more than one row, they will
# appear together in the data frame
locus2pwy=locus2pwy[with(locus2pwy,
                         order(gene,Pathway.id)),]
# some genes have multiple transcripts, which means
# we might get some rows that have the same locus and
# pathway id - get rid of the duplicates
uniques=!duplicated(locus2pwy[,c('gene','Pathway.id')])
locus2pwy=locus2pwy[uniques,]
towrite=paste0('results/locus2pathway_',
               s,'_',p,'.tsv')
# write the data to disk
write.table(locus2pwy,towrite,quote=F,row.names=F,sep='\t')
system(paste('gzip -f',towrite))
# write supplemental data file
pth='../ManuscriptsSupplementalDataFiles/SupplementalDataFiles/S2-Pathway'
towrite=file.path(pth,paste0('locus2pathway_',s,'_',p,'.tsv'))
write.table(locus2pwy,towrite,quote=F,row.names=F,sep='\t')
system(paste('gzip -f',towrite))
```

Note: At lower scov thresholds, CUFF.12644  was annotated as "SIMILAR TO GIO2-1930-MONOMER (Cre06.g288750.t1.2) peptidylprolyl isomerase (scov=47.6,ident=42.3)" (from Chlamydomonas reinhardtii) but this annotation is probably wrong. The nr blastx found that this gene encodes an SC35 like splicing factor, similar to AT1G55310 AT-SCL33 (acidic N-terminal charged extension, RRM, then RS domain). The coverage threshold is probably too low resulting in a loss of information when the PlantCyc annotation supercedes the blast "nr" annotation. 

It would be helpful to know the size distribution of conserved domains (like ATP-binding cassette, for instance) that occur in many proteins of diverse function. 

Improve BED14 annotations
---------------------------------------------------

Now that we have some matches to plantcyc enzymes, let's write out a better version of the blueberry gene models BED14 file. We'll distribute this on the blueberry quickload site so that users can search for pathway and enzyme names to find genes of interest in IGB.

```{r}
infile=paste0('results/filtered_plantcyc_results_',
              s,'_',p,'.tsv.gz')
cmd=paste('./src/makeBed.py',
          '-p',infile,
          '-b',bedfile)
bed14=read.delim(pipe(cmd),header=F,sep='\t',
                na.strings='NA',quote='',as.is=T)
outfn=file.path('results','V_corymbosum_scaffold_May_2013_wDescrPwy.bed')
write.table(bed14,outfn,sep='\t',row.names=F,quote=F,col.names=F)
```

Conclusion
----------

There were many genes that could be annotated with PlantCyc enzymes and pathways.

Limitations of the Analysis
----

The blastx filtering was somewhat ad hoc. There may be a better approach. Using lower coverage may introduce many false positives, but it is clearly important to include the overall percent identity and subject coverage in annotations to enable researchers to assess the homologies.

Session info
----

```{r}
sessionInfo()
```


