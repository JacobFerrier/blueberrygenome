#!/usr/bin/env python

ex=\
"""
Add a new columns: 
  subject id
  Phytozome Id (if available)
  protein name
  species
"""

import sys,optparse,fileinput,re

def main(args):
    onfirst=True
    for line in fileinput.input(args):
        if onfirst:
            line = '\t'.join([line.rstrip(),
                              's.id',
                              'ph.id',
                              'protein',
                              'species'])+'\n'
            onfirst=False
            sys.stdout.write(line)
            continue
        toks=line.rstrip().split('\t')
        title=toks[14]
        if title=='NA':
            sid='NA'
            phyto_id='NA'
            protein_name='NA'
            species='NA'
        else:
            toks=map(lambda x:x.strip(),title.split(' | '))
            sid=toks[0]
            phyto_id=re.sub('Phytozome: ','',toks[1])
            if phyto_id=='':
                raise ValueError(sid)
            protein_name=re.sub(phyto_id+'; ','',toks[2])
            protein_name=re.sub('No-protein-common-name; ','',protein_name)
            if protein_name=='':
                raise ValueError(sid)
            species=re.sub('Species: ','',toks[3])
            if species=='':
                raise ValueError(sid)
            if species=='Arabidopsis thaliana':
                gene=phyto_id.split('.')[0]
                protein_name=re.sub(gene+'; ','',protein_name)
        line='\t'.join([line.rstrip(),
                        sid,
                        phyto_id,
                        protein_name,
                        species])+'\n'
        sys.stdout.write(line)

if __name__ == '__main__':
    usage='%prog [ file ... ]\n'+ex
    parser=optparse.OptionParser(usage)
    (options,args)=parser.parse_args()
    main(args=args)

