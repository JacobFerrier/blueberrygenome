Does splicing produce alternative Enzymes? 
========================================================

*Note* This is copied from an HTML file created by Ivory.

Since we used individual transcripts to compare to to proteins/enzymes, it is theoretically possible that the alternate transcripts produced by the same gene could be noted as similar to differente enzymes.

Read in a file that links transcripts and genes to plantcyc proteins.

```{r}
f = "results/filtered_plantcyc_results_45.tsv.gz"
p <- read.delim(file = f, header = T, sep = "\t", 
                quote = "",
                as.is=T)
dim(p)
```

Get rid of columns we don't need and make an id using gene and protein names:

```{r}
p <- p[, c("gene", "q.id", "protein")]
p$id = paste(p$gene, p$protein, sep = ":")
dim(p)
```

Get rid of duplicates:

```{r}
p2 <- p[!duplicated(p$id), ]
dim(p2)
```

If there are any rows with the same gene, they must have different ids:

```{r}
genes <- p2[duplicated(p2$gene),]$gene
length(unique(genes))
```

Extract the rows with genes that map to different proteins:

```{r}
v=p$gene%in%genes
p4=p[v,]
o=order(p4$gene,p4$q.id)
p4=p4[o,c('gene','q.id','id','protein')]
dim(p4)
```

Get rid of identical rows (this happened because some enzymes mapped to multiple pathways):


```{r}
p5=unique(p4[,c('gene','q.id','protein')])
row.names(p5)=p5$q.id
p5[,c('gene','protein')]
```

So the basic answer is YES. I'd like to look at any earlier file we might have that had all/more hits for each transcript, and see how many instances there were of a gene coding for mulitple ontologically different products, before we applied filters for the 'best' match of the lot.

Since there ARE genes with separate transcripts coding for different proteins, I think the next logical steps are to: a) Look at the transcripts in IGB, and see how much splicing affects the protein sequence. b) Look at the experimental evidence for the enzyme that each transcript is being compared to and answer the question: “In the organism in which these different enzymes have been studied experimentally, are these enzymes thougth to be the alternative products of the same gene?” c) Ultimately answering the questions: “Do we really beleive that these are instances of splicing producing products with different functions (different enought to have different protein names)? and if so, is that interesting enought to be worth talking about?”

```{r}
sessionInfo()
```